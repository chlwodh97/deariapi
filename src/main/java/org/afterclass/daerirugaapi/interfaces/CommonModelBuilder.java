package org.afterclass.daerirugaapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
