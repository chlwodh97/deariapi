package org.afterclass.daerirugaapi.service;

import lombok.RequiredArgsConstructor;
import org.afterclass.daerirugaapi.entity.Member;
import org.afterclass.daerirugaapi.model.member.MemberItem;
import org.afterclass.daerirugaapi.model.member.MemberRequest;
import org.afterclass.daerirugaapi.model.member.MemberResponse;
import org.afterclass.daerirugaapi.model.result.ListResult;
import org.afterclass.daerirugaapi.repository.MemberRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    // 회원 C
    public void setMember(MemberRequest request) {
        memberRepository.save(new Member.Builder(request).build());
    }

    // 회원 복수 R
    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();
        for (Member member : originList) result.add(new MemberItem.Builder(member).build());
        return result;
    }

    // 회원 복수 페이징 R
    public ListResult<MemberItem> getMembersPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Member> memberPage = memberRepository.findAll(pageRequest);
        List<MemberItem> result = new LinkedList<>();
        for (Member member : memberPage) result.add(new MemberItem.Builder(member).build());
        return ListConvertService.settingListResult(result, memberPage.getTotalElements(), memberPage.getTotalPages(), memberPage.getPageable().getPageNumber());
    }

    // 회원 단수 R
    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        return new MemberResponse.Builder(originData).build();
    }

    // 회원 U
    public void putMember(long id, MemberRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.putMember(request);
        memberRepository.save(originData);
    }

    // 회원 D
    public void delMember(long id) {
        memberRepository.deleteById(id);
    }

}
