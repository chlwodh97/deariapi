package org.afterclass.daerirugaapi.service;

import lombok.RequiredArgsConstructor;
import org.afterclass.daerirugaapi.entity.Member;
import org.afterclass.daerirugaapi.entity.point.PointHistory;
import org.afterclass.daerirugaapi.model.point.pointHistory.PointHistoryItem;
import org.afterclass.daerirugaapi.model.point.pointHistory.PointHistoryRequest;
import org.afterclass.daerirugaapi.model.point.pointHistory.PointHistoryResponse;
import org.afterclass.daerirugaapi.model.result.ListResult;
import org.afterclass.daerirugaapi.repository.MemberRepository;
import org.afterclass.daerirugaapi.repository.PointHistoryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PointHistoryService {
    private final PointHistoryRepository pointHistoryRepository;
    private final MemberRepository memberRepository;

    /**
     * 포인트 내역 등록
     */
    public void setPointHistory(PointHistoryRequest request) {
        Member memberData = memberRepository.findById(request.getMemberId()).orElseThrow();
        pointHistoryRepository.save(new PointHistory.Builder(request, memberData).build());
    }

    /**
     * 포인트 내역 리스트 보기
     */
    public List<PointHistoryItem> getPointHistorys() {
        List<PointHistory> originList = pointHistoryRepository.findAll();
        List<PointHistoryItem> result = new LinkedList<>();
        for (PointHistory pointHistory : originList) result.add(new PointHistoryItem.Builder(pointHistory).build());
        return result;
    }

    /**
     * 포인트 내역 리스트 보기 페이징
     */
    public ListResult<PointHistoryItem> getPointHistorysPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<PointHistory> pointHistoryPage = pointHistoryRepository.findAll(pageRequest);
        List<PointHistoryItem> result = new LinkedList<>();
        for (PointHistory pointHistory : pointHistoryPage) result.add(new PointHistoryItem.Builder(pointHistory).build());
        return ListConvertService.settingListResult(result, pointHistoryPage.getTotalElements(), pointHistoryPage.getTotalPages(), pointHistoryPage.getPageable().getPageNumber());
    }

    /**
     * 포인트 내역 상세보기
     */
    public PointHistoryResponse getPointHistory(long id) {
        PointHistory originData = pointHistoryRepository.findById(id).orElseThrow();
        return new PointHistoryResponse.Builder(originData).build();
    }

    /**
     * 포인트 내역 수정
     */
    public void putPointHistory(PointHistoryRequest request, long id) {
        Member memberData = memberRepository.findById(request.getMemberId()).orElseThrow();
        PointHistory originData = pointHistoryRepository.findById(id).orElseThrow();
        originData.putPointHistory(request, memberData);
        pointHistoryRepository.save(originData);
    }

    /**
     * 포인트 내역 삭제
     */
    public void delPointHistory(long id) {
        pointHistoryRepository.deleteById(id);
    }
}
