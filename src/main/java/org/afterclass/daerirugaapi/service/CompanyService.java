package org.afterclass.daerirugaapi.service;

import lombok.RequiredArgsConstructor;
import org.afterclass.daerirugaapi.entity.Company;
import org.afterclass.daerirugaapi.model.company.CompanyRequest;
import org.afterclass.daerirugaapi.repository.CompanyRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CompanyService {
    private final CompanyRepository companyRepository;

    /**
     * 숙박업체 등록 -> 초기 위도 경도0
     */
    public void setCompany(CompanyRequest request) {
        companyRepository.save(new Company.Builder(request).build());
    }
}
