package org.afterclass.daerirugaapi.service;

import lombok.RequiredArgsConstructor;
import org.afterclass.daerirugaapi.entity.Member;
import org.afterclass.daerirugaapi.entity.point.Point;
import org.afterclass.daerirugaapi.model.point.point.PointItem;
import org.afterclass.daerirugaapi.model.point.point.PointRequest;
import org.afterclass.daerirugaapi.model.point.point.PointResponse;
import org.afterclass.daerirugaapi.model.result.ListResult;
import org.afterclass.daerirugaapi.repository.MemberRepository;
import org.afterclass.daerirugaapi.repository.PointRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PointService {
    private final PointRepository pointRepository;
    private final MemberRepository memberRepository;

    /**
     * 포인트 등록
     */
    public void setPoint(PointRequest request) {
        Member memberData = memberRepository.findById(request.getMemberId()).orElseThrow();
        pointRepository.save(new Point.Builder(request, memberData).build());
    }

    /**
     *  포인트 리스트 보기
     */
    public List<PointItem> getPoints() {
        List<Point> originData = pointRepository.findAll();
        List<PointItem> result = new LinkedList<>();
        for (Point point : originData) result.add(new PointItem.Builder(point).build());
        return result;
    }

    /**
     * 포인트 리스트 보기 페이징
     */
    public ListResult<PointItem> getPointsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Point> pointPage = pointRepository.findAll(pageRequest);
        List<PointItem> result = new LinkedList<>();
        for (Point point : pointPage) result.add(new PointItem.Builder(point).build());
        return ListConvertService.settingListResult(result, pointPage.getTotalElements(), pointPage.getTotalPages(), pointPage.getPageable().getPageNumber());
    }

    /**
     * 포인트 상세보기
     */
    public PointResponse getPoint(long id) {
        Point originData = pointRepository.findById(id).orElseThrow();
        return new PointResponse.Builder(originData).build();
    }

    /**
     * 포인트 수정
     */
    public void putPoint(long id, PointRequest request) {
        Member memberData = memberRepository.findById(request.getMemberId()).orElseThrow();
        Point originData = pointRepository.findById(id).orElseThrow();
        originData.putPoint(request, memberData);
        pointRepository.save(originData);
    }

    /**
     * 포인트 삭제
     */
    public void delPoint(long id) {
        pointRepository.deleteById(id);
    }
}
