package org.afterclass.daerirugaapi.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Schema(description = "IN = 입금, OUT = 출금")
public enum PointType {

    IN("입금"),
    OUT("출금");

    private final String pointTypeName;
}
