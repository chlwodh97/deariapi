package org.afterclass.daerirugaapi.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Schema(description = "JUNMEMBER = 준회원, JUNGMEMEBER = 정회원, ADMIN = 관리자")
public enum MemberClass {
    JUNMEMBER("준회원"),
    JUNGMEMEBER("정회원"),
    ADMIN("관리자");

    private final String memberState;
}
