package org.afterclass.daerirugaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DaerirugaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DaerirugaApiApplication.class, args);
	}

}
