package org.afterclass.daerirugaapi.model.company;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyRequest {
    private String companyName;
    private String callNumber;
    private String address;
    private String companyImage;
    private String companyDetail;
}
