package org.afterclass.daerirugaapi.model.point.point;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.afterclass.daerirugaapi.entity.point.Point;
import org.afterclass.daerirugaapi.interfaces.CommonModelBuilder;
import org.afterclass.daerirugaapi.model.point.pointHistory.PointHistoryItem;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PointItem {

    @Schema(description = "포인트 id")
    private Long id;

    @Schema(description = "회원 id")
    private Long memberId;

    @Schema(description = "보유포인트")
    private Short memberPoint;

    private PointItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberPoint = builder.memberPoint;
    }

    public static class Builder implements CommonModelBuilder<PointItem> {
        private final Long id;
        private final Long memberId;
        private final Short memberPoint;

        public Builder(Point point) {
            this.id = point.getId();
            this.memberId = point.getMember().getId();
            this.memberPoint = point.getMemberPoint();
        }

        @Override
        public PointItem build() {
            return new PointItem(this);
        }
    }
}
