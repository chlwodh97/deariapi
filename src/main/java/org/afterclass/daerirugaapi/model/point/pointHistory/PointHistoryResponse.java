package org.afterclass.daerirugaapi.model.point.pointHistory;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.afterclass.daerirugaapi.entity.Member;
import org.afterclass.daerirugaapi.entity.point.PointHistory;
import org.afterclass.daerirugaapi.enums.PointType;
import org.afterclass.daerirugaapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PointHistoryResponse {

    @Schema(description = "포인트내역 id")
    private Long id;

    @Schema(description = "회원 id")
    private Long memberId;

    @Schema(description = "포인트 입출금여부")
    private PointType pointType;

    @Schema(description = "이용내역")
    private String content;

    @Schema(description = "포인트")
    private Short point;

    @Schema(description = "입출금 날짜")
    private LocalDateTime dateUpdate;

    private PointHistoryResponse(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.pointType = builder.pointType;
        this.content = builder.content;
        this.point = builder.point;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class Builder implements CommonModelBuilder<PointHistoryResponse> {
        private final Long id;
        private final Long memberId;
        private final PointType pointType;
        private final String content;
        private final Short point;
        private final LocalDateTime dateUpdate;

        public Builder(PointHistory pointHistory) {
            this.id = pointHistory.getId();
            this.memberId = pointHistory.getMember().getId();
            this.pointType = pointHistory.getPointType();
            this.content = pointHistory.getContent();
            this.point = pointHistory.getPoint();
            this.dateUpdate = pointHistory.getDateUpdate();
        }

        @Override
        public PointHistoryResponse build() {
            return new PointHistoryResponse(this);
        }
    }
}
