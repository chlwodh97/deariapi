package org.afterclass.daerirugaapi.model.point.point;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PointRequest {
    @NotNull
    @Schema(description = "회원 id")
    private Long memberId;

    @NotNull
    @Schema(description = "보유포인트")
    private Short memberPoint;
}
