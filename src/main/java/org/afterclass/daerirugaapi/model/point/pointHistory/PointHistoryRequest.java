package org.afterclass.daerirugaapi.model.point.pointHistory;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.afterclass.daerirugaapi.entity.Member;
import org.afterclass.daerirugaapi.enums.PointType;

import java.time.LocalDateTime;

@Getter
@Setter
public class PointHistoryRequest {
    @NotNull
    @Schema(description = "회원 id")
    private Long memberId;

    @NotNull
    @Schema(description = "포인트 입출금여부")
    private PointType pointType;

    @Schema(description = "이용내역")
    private String content;

    @NotNull
    @Schema(description = "포인트")
    private Short point;


}
