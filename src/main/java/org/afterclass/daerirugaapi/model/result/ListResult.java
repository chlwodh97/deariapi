package org.afterclass.daerirugaapi.model.result;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private String msg;
    private Integer code;
    private List<T> list;
    private Long totalCount;
    private Integer totalPage;
    private Integer currentPage;
}
