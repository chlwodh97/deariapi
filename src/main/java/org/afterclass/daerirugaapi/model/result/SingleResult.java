package org.afterclass.daerirugaapi.model.result;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult{
    private String msg;
    private Integer code;
    private T data;
}
