package org.afterclass.daerirugaapi.model.member;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.afterclass.daerirugaapi.entity.Member;
import org.afterclass.daerirugaapi.enums.MemberClass;
import org.afterclass.daerirugaapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {

    @Schema(description = "회원 id")
    private Long id;

    @Schema(description = "회원 이름")
    private String memberName;

    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @Schema(description = "성별")
    private Boolean isMan;

    @Schema(description = "사업장명")
    private String workPlace;

    @Schema(description = "등급")
    private MemberClass memberClass;

    private MemberItem(Builder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
        this.isMan = builder.isMan;
        this.workPlace = builder.workPlace;
        this.dateBirth = builder.dateBirth;
        this.memberClass = builder.memberClass;
    }

    public static class Builder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String memberName;
        private final LocalDate dateBirth;
        private final Boolean isMan;
        private final String workPlace;
        private final MemberClass memberClass;

        public Builder(Member member) {
            this.id = member.getId();
            this.memberName = member.getMemberName();
            this.isMan = member.getIsMan();
            this.workPlace = member.getWorkPlace();
            this.dateBirth = member.getDateBirth();
            this.memberClass = member.getMemberClass();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
