package org.afterclass.daerirugaapi.model.member;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.afterclass.daerirugaapi.enums.MemberClass;

@Getter
@Setter
public class MemberPutClassRequest {
    @Schema(description = "등급")
    private MemberClass memberClass;
}
