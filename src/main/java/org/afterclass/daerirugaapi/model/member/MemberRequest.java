package org.afterclass.daerirugaapi.model.member;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.afterclass.daerirugaapi.enums.MemberClass;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
public class MemberRequest {
    @NotNull
    @Schema(description = "회원 이름", minLength = 2, maxLength = 20)
    @Length(min = 2, max = 20)
    private String memberName;

    @NotNull
    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @NotNull
    @Schema(description = "전화번호", minLength = 11, maxLength = 13)
    @Length(min = 11, max = 13)
    private String callNumber;

    @NotNull
    @Schema(description = "성별")
    private Boolean isMan;

    @NotNull
    @Schema(description = "아이디", minLength = 3, maxLength = 20)
    @Length(min = 3, max = 20)
    private String username;

    @NotNull
    @Schema(description = "비밀번호")
    private String password;

    @NotNull
    @Schema(description = "주소", minLength = 10, maxLength = 100)
    @Length(min = 10, max = 100)
    private String address;

    @NotNull
    @Schema(description = "사업장명", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String workPlace;

    @NotNull
    @Schema(description = "사업장 연락처", minLength = 11, maxLength = 13)
    @Length(min = 11, max = 13)
    private String workPlaceCall;
}
