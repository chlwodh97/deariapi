package org.afterclass.daerirugaapi.model.member;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.afterclass.daerirugaapi.entity.Member;
import org.afterclass.daerirugaapi.enums.MemberClass;
import org.afterclass.daerirugaapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberResponse {

    @Schema(description = "회원 id")
    private Long id;

    @Schema(description = "회원 이름")
    private String memberName;

    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @Schema(description = "전화번호")
    private String callNumber;

    @Schema(description = "주소")
    private String address;

    @Schema(description = "사업장명")
    private String workPlace;

    @Schema(description = "사업장 연락처")
    private String workPlaceCall;

    @Schema(description = "등급")
    private MemberClass memberClass;

    private MemberResponse(Builder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
        this.dateBirth = builder.dateBirth;
        this.callNumber = builder.callNumber;
        this.address = builder.address;
        this.workPlace = builder.workPlace;
        this.workPlaceCall = builder.workPlaceCall;
        this.memberClass = builder.memberClass;
    }

    public static class Builder implements CommonModelBuilder<MemberResponse> {
        private final Long id;
        private final String memberName;
        private final LocalDate dateBirth;
        private final String callNumber;
        private final String address;
        private final String workPlace;
        private final String workPlaceCall;
        private final MemberClass memberClass;

        public Builder(Member member) {
            this.id = member.getId();
            this.memberName = member.getMemberName();
            this.dateBirth = member.getDateBirth();
            this.callNumber = member.getCallNumber();
            this.address = member.getAddress();
            this.workPlace = member.getWorkPlace();
            this.workPlaceCall = member.getWorkPlaceCall();
            this.memberClass = member.getMemberClass();
        }

        @Override
        public MemberResponse build() {
            return new MemberResponse(this);
        }
    }
}
