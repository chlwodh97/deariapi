package org.afterclass.daerirugaapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.afterclass.daerirugaapi.interfaces.CommonModelBuilder;
import org.afterclass.daerirugaapi.model.company.CompanyRequest;


@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String companyName;

    @Column(nullable = false, length = 13)
    private String callNumber;

    @Column(nullable = false, length = 13)
    private Double latitude;

    @Column(nullable = false, length = 13)
    private Double longitude;

    @Column(nullable = false, length = 50)
    private String address;

    @Column(nullable = false)
    private String companyImage;

    private String companyDetail;

    private Company(Builder builder) {
        this.companyName = builder.companyName;
        this.callNumber = builder.callNumber;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.address = builder.address;
        this.companyImage = builder.companyImage;
        this.companyDetail = builder.companyDetail;
    }

    public static class Builder implements CommonModelBuilder<Company> {
        private final String companyName;
        private final String callNumber;
        private final Double latitude;
        private final Double longitude;
        private final String address;
        private final String companyImage;
        private final String companyDetail;

        public Builder(CompanyRequest request) {
            this.companyName = request.getCompanyName();
            this.callNumber = request.getCallNumber();
            this.latitude = 0D;
            this.longitude = 0D;
            this.address = request.getAddress();
            this.companyImage = request.getCompanyImage();
            this.companyDetail = request.getCompanyDetail();
        }

        @Override
        public Company build() {
            return new Company(this);
        }
    }
}
