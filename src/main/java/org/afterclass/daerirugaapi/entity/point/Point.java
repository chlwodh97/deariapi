package org.afterclass.daerirugaapi.entity.point;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.afterclass.daerirugaapi.entity.Member;
import org.afterclass.daerirugaapi.interfaces.CommonModelBuilder;
import org.afterclass.daerirugaapi.model.point.point.PointRequest;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Point {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원 id FK
    @JoinColumn(nullable = false, name = "memberId")
    @ManyToOne(fetch = FetchType.EAGER)
    private Member member;

    // 보유포인트
    @Column(nullable = false)
    private Short memberPoint;

    public void putPoint(PointRequest request, Member member) {
        this.member = member;
        this.memberPoint = request.getMemberPoint();
    }

    private Point(Builder builder) {
        this.member = builder.member;
        this.memberPoint = builder.memberPoint;
    }

    public static class Builder implements CommonModelBuilder<Point> {
        private final Member member;
        private final Short memberPoint;

        public Builder(PointRequest request, Member member) {
            this.member = member;
            this.memberPoint = request.getMemberPoint();
        }

        @Override
        public Point build() {
            return new Point(this);
        }
    }
}

