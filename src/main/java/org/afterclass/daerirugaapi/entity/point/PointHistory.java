package org.afterclass.daerirugaapi.entity.point;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.afterclass.daerirugaapi.entity.Member;
import org.afterclass.daerirugaapi.enums.PointType;
import org.afterclass.daerirugaapi.interfaces.CommonModelBuilder;
import org.afterclass.daerirugaapi.model.point.pointHistory.PointHistoryRequest;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PointHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원 id FK
    @JoinColumn(nullable = false, name = "memberId")
    @ManyToOne(fetch = FetchType.LAZY)
    private Member member;

    // 포인트 입출금여부
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PointType pointType;

    // 이용내역
    private String content;

    // 포인트
    @Column(nullable = false)
    private Short point;

    // 입출금 날짜
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putPointHistory(PointHistoryRequest request, Member member) {
        this.member = member;
        this.point = request.getPoint();
        this.pointType = request.getPointType();
    }

    private PointHistory(Builder builder) {
        this.member = builder.member;
        this.pointType = builder.pointType;
        this.content = builder.content;
        this.point = builder.point;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class Builder implements CommonModelBuilder<PointHistory> {
        private final Member member;
        private final PointType pointType;
        private String content;
        private final Short point;
        private final LocalDateTime dateUpdate;

        public Builder(PointHistoryRequest request, Member member) {
            this.member = member;
            this.pointType = request.getPointType();
            this.content = request.getContent();
            this.point = request.getPoint();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public PointHistory build() {
            return new PointHistory(this);
        }
    }
}
