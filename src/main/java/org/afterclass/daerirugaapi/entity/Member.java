package org.afterclass.daerirugaapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.afterclass.daerirugaapi.enums.MemberClass;
import org.afterclass.daerirugaapi.interfaces.CommonModelBuilder;
import org.afterclass.daerirugaapi.model.member.MemberPutClassRequest;
import org.afterclass.daerirugaapi.model.member.MemberRequest;

import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원 이름
    @Column(nullable = false, length = 20)
    private String memberName;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 전화번호
    @Column(nullable = false, length = 13, unique = true)
    private String callNumber;

    // 성별
    @Column(nullable = false)
    private Boolean isMan;

    // 아이디
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    // 비밀번호
    @Column(nullable = false)
    private String password;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 사업장명
    @Column(nullable = false, length = 30)
    private String workPlace;

    // 사업장 연락처
    @Column(nullable = false, length = 13)
    private String workPlaceCall;

    // 등급 (기본값 = 준회원)
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private MemberClass memberClass;

    /**
     * 멤버 등급 변경 (관리자만 가능)
     */
    public void putMemberClass(MemberPutClassRequest request) {
        this.memberClass = request.getMemberClass();
    }

    /**
     * 멤버 정보 변경(아이디랑 비밀번호는 따로 할지 고민중)
     */

    public void putMember(MemberRequest request) {
        this.memberName = request.getMemberName();
        this.dateBirth = request.getDateBirth();
        this.callNumber = request.getCallNumber();
        this.isMan = request.getIsMan();
        this.username = request.getUsername();
        this.password = request.getPassword();
        this.address = request.getAddress();
        this.workPlace = request.getWorkPlace();
        this.workPlaceCall = request.getWorkPlaceCall();
    }

    private Member(Builder builder) {
        this.memberName = builder.memberName;
        this.dateBirth = builder.dateBirth;
        this.callNumber = builder.callNumber;
        this.isMan = builder.isMan;
        this.username = builder.username;
        this.password = builder.password;
        this.address = builder.address;
        this.workPlace = builder.workPlace;
        this.workPlaceCall = builder.workPlaceCall;
        this.memberClass = builder.memberClass;
    }

    public static class Builder implements CommonModelBuilder<Member> {
        private final String memberName;
        private final LocalDate dateBirth;
        private final String callNumber;
        private final Boolean isMan;
        private final String username;
        private final String password;
        private final String address;
        private final String workPlace;
        private final String workPlaceCall;
        private final MemberClass memberClass;

        public Builder(MemberRequest request) {
            this.memberName = request.getMemberName();
            this.dateBirth = request.getDateBirth();
            this.callNumber = request.getCallNumber();
            this.isMan = request.getIsMan();
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.address = request.getAddress();
            this.workPlace = request.getWorkPlace();
            this.workPlaceCall = request.getWorkPlaceCall();
            this.memberClass = MemberClass.JUNMEMBER;
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
