package org.afterclass.daerirugaapi.repository;

import org.afterclass.daerirugaapi.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
