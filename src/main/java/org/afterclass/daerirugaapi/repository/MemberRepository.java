package org.afterclass.daerirugaapi.repository;

import org.afterclass.daerirugaapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
