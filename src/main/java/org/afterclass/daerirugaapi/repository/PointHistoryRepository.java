package org.afterclass.daerirugaapi.repository;

import org.afterclass.daerirugaapi.entity.point.PointHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointHistoryRepository extends JpaRepository<PointHistory, Long> {
}
