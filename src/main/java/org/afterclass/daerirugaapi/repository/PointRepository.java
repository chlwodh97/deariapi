package org.afterclass.daerirugaapi.repository;

import org.afterclass.daerirugaapi.entity.point.Point;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointRepository extends JpaRepository<Point, Long> {
}
