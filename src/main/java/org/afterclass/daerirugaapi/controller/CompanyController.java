package org.afterclass.daerirugaapi.controller;


import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.afterclass.daerirugaapi.model.company.CompanyRequest;
import org.afterclass.daerirugaapi.model.result.CommonResult;
import org.afterclass.daerirugaapi.service.CompanyService;
import org.afterclass.daerirugaapi.service.ResponseService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/company")
public class CompanyController {
    private final CompanyService companyService;

    @PostMapping("/new")
    @Operation(summary = "숙박업체 등록")
    public CommonResult setCompany(@RequestBody CompanyRequest request) {
        companyService.setCompany(request);
        return ResponseService.getSuccessResult();
    }
}
