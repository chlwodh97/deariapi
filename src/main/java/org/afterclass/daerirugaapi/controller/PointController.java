package org.afterclass.daerirugaapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afterclass.daerirugaapi.model.point.point.PointItem;
import org.afterclass.daerirugaapi.model.point.point.PointRequest;
import org.afterclass.daerirugaapi.model.point.point.PointResponse;
import org.afterclass.daerirugaapi.model.result.CommonResult;
import org.afterclass.daerirugaapi.model.result.ListResult;
import org.afterclass.daerirugaapi.model.result.SingleResult;
import org.afterclass.daerirugaapi.service.ListConvertService;
import org.afterclass.daerirugaapi.service.PointService;
import org.afterclass.daerirugaapi.service.ResponseService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/point")
@Tag(name = "Point", description = "포인트")
public class PointController {
    private final PointService pointService;

    // 포인트 C
    @PostMapping("/new")
    @Operation(summary = "포인트 등록")
    public CommonResult setPoint(@RequestBody @Valid PointRequest request) {
        pointService.setPoint(request);
        return ResponseService.getSuccessResult();
    }

    /**
     * 포인트 복수 R
     */
    @GetMapping("/all")
    @Operation(summary = "포인트 리스트 보기")
    public ListResult<PointItem> getPoints() {
        return ListConvertService.settingListResult(pointService.getPoints());
    }

    /**
     * 포인트 복수 R 페이징
     */
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "포인트 리스트 보기 페이징")
    public ListResult<PointItem> getPointsPage(@PathVariable int pageNum) {
        return pointService.getPointsPage(pageNum);
    }

    // 포인트 단수 R
    @GetMapping("/detail/pointId/{pointId}")
    @Operation(summary = "포인트 상세보기")
    public SingleResult<PointResponse> getPoint(@PathVariable long pointId) {
        return ResponseService.getSingleResult(pointService.getPoint(pointId));
    }

    // 포인트 U
    @PutMapping("/changeInfo/pointId/{pointId}")
    @Operation(summary = "포인트 정보 수정")
    public CommonResult putPoint(@PathVariable long pointId, @RequestBody @Valid PointRequest request) {
        pointService.putPoint(pointId, request);
        return ResponseService.getSuccessResult();
    }

    // 포인트 U
    @DeleteMapping("/delete/pointId/{pointId}")
    @Operation(summary = "포인트 정보 삭제")
    public CommonResult delMember(@PathVariable long pointId) {
        pointService.delPoint(pointId);
        return ResponseService.getSuccessResult();
    }
}
