package org.afterclass.daerirugaapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afterclass.daerirugaapi.model.member.MemberItem;
import org.afterclass.daerirugaapi.model.member.MemberRequest;
import org.afterclass.daerirugaapi.model.member.MemberResponse;
import org.afterclass.daerirugaapi.model.result.CommonResult;
import org.afterclass.daerirugaapi.model.result.ListResult;
import org.afterclass.daerirugaapi.model.result.SingleResult;
import org.afterclass.daerirugaapi.service.ListConvertService;
import org.afterclass.daerirugaapi.service.MemberService;
import org.afterclass.daerirugaapi.service.ResponseService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
@Tag(name = "Member", description = "회원")
public class MemberController {
    private final MemberService memberService;

    // 회원 C
    @PostMapping("/new")
    @Operation(summary = "회원 등록")
    public CommonResult setMember(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(request);
        return ResponseService.getSuccessResult();
    }

    // 회원 복수 R
    @GetMapping("/all")
    @Operation(summary = "회원 리스트 전체보기")
    public ListResult<MemberItem> getMembers() {
        return ListConvertService.settingListResult(memberService.getMembers());
    }

    // 회원 복수 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "회원 리스트 전체보기 (페이징)")
    public ListResult<MemberItem> getMembersPage(@PathVariable int pageNum) {
        return memberService.getMembersPage(pageNum);
    }

    // 회원 단수 R
    @GetMapping("/detail/memberId/{memberId}")
    @Operation(summary = "회원 상세보기")
    public SingleResult<MemberResponse> getMember(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberService.getMember(memberId));
    }

    // 회원 U
    @PutMapping("/changeInfo/memberId/{memberId}")
    @Operation(summary = "회원 정보 수정")
    public CommonResult putMember(@PathVariable long memberId, @RequestBody @Valid MemberRequest request) {
        memberService.putMember(memberId, request);
        return ResponseService.getSuccessResult();
    }

    // 회원 U
    @DeleteMapping("/delete/memberId/{memberId}")
    @Operation(summary = "회원 정보 삭제")
    public CommonResult delMember(@PathVariable long memberId) {
        memberService.delMember(memberId);
        return ResponseService.getSuccessResult();
    }
}
