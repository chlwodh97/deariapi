package org.afterclass.daerirugaapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.afterclass.daerirugaapi.model.point.pointHistory.PointHistoryItem;
import org.afterclass.daerirugaapi.model.point.pointHistory.PointHistoryRequest;
import org.afterclass.daerirugaapi.model.point.pointHistory.PointHistoryResponse;
import org.afterclass.daerirugaapi.model.result.CommonResult;
import org.afterclass.daerirugaapi.model.result.ListResult;
import org.afterclass.daerirugaapi.model.result.SingleResult;
import org.afterclass.daerirugaapi.service.ListConvertService;
import org.afterclass.daerirugaapi.service.PointHistoryService;
import org.afterclass.daerirugaapi.service.ResponseService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pointHistory")
@Tag(name = "PointHistory", description = "포인트 내역")
public class PointHistoryController {
    private final PointHistoryService pointHistoryService;

    /**
     * 포인트 내역 등록
     */
    @PostMapping("/new")
    @Operation(description = "포인트 내역 등록")
    public CommonResult setPointHistory(@RequestBody @Valid PointHistoryRequest request) {
        pointHistoryService.setPointHistory(request);
        return ResponseService.getSuccessResult();
    }

    /**
     * 포인트 내역 리스트 보기
     */
    @GetMapping("/all")
    @Operation(description = "포인트 내역 리스트 보기")
    public ListResult<PointHistoryItem> getPointHistorys() {
        return ListConvertService.settingListResult(pointHistoryService.getPointHistorys());
    }

    /**
     * 포인트 내역 리스트 보기 페이징
     */
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(description = "포인트 내역 리스트 보기 페이징")
    public ListResult<PointHistoryItem> getPointHistorysPage(@PathVariable int pageNum) {
        return pointHistoryService.getPointHistorysPage(pageNum);
    }

    /**
     * 포인트 내역 상세보기
     */
    @GetMapping("/detail/point-History-Id/{pointHistoryId}")
    @Operation(description = "포인트 내역 상세보기")
    public SingleResult<PointHistoryResponse> getPointHistory(@PathVariable long pointHistoryId) {
        return ResponseService.getSingleResult(pointHistoryService.getPointHistory(pointHistoryId));
    }

    /**
     * 포인트 내역 수정
     */
    @PutMapping("/changeInfo/point-History-Id/{pointHistoryId}")
    @Operation(description = "포인트 내역 수정")
    public CommonResult putPointHistory(@RequestBody @Valid PointHistoryRequest request, @PathVariable long pointHistoryId) {
        pointHistoryService.putPointHistory(request, pointHistoryId);
        return ResponseService.getSuccessResult();
    }

    /**
     * 포인트 내역 삭제
     */
    @DeleteMapping("/delete/point-History-Id/{pointHistoryId}")
    @Operation(description = "포인트 내역 삭제")
    public CommonResult deletePointHistory(@PathVariable long pointHistoryId) {
        pointHistoryService.delPointHistory(pointHistoryId);
        return ResponseService.getSuccessResult();
    }
}
